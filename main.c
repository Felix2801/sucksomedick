#include <stdio.h>
#include <stdlib.h>
#include "runMenu.h"

int main(int argc, char *argv[])
{
    printf("Started\n");

    if (argc != 2)
    {
        printf("Wrong number of arguments!\n");
        return 1;
    }

    char *filename = argv[1];

    runMenu();

    return 0;
}